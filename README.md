# Device Tree for Meizu M6 Note (m1721)

## Spec Sheet

| Feature                 | Specification                     |
| :---------------------- | :-------------------------------- |
| CPU                     | Octa-core 2.0 GHz Cortex-A53      |
| Chipset                 | Qualcomm MSM8953 Snapdragon 625   |
| GPU                     | Adreno 506                        |
| Memory                  | 3/4 GB                            |
| Shipped Android Version | 7.1.2                             |
| Storage                 | 16/32/64 GB                       |
| MicroSD                 | Up to 256 GB                      |
| Battery                 | 5000 mAh (non-removable)          |
| Dimensions              | 151 x 76 x 8.5 mm                 |
| Display                 | 1920x1080 pixels, 5.5 (~401 PPI)  |
| Rear Camera             | 13 MP, 5 MP, LED flash            |
| Front Camera            | 15 MP                             |
| Release Date            | December 2017                     |
